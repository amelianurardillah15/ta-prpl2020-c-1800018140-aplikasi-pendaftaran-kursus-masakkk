<!DOCTYPE html>
<html>
  <head>
    <title>Kursus Memasak Official</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:300,400,700,800|Open+Sans:300,400,700" rel="stylesheet">
    <script type="text/javascript" src="cari.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

          <h4><a href="index.html" class="site-logo">Home</a></h4>
          <p align="left"><a href="caridatap.php"><li>Mencari Data Pendaftar</li></a></p>

        </div>
        </li>
    </head>

     <body style="background-color: #DCDCDC"><center>

      <h2> DATA PESERTA KURSUS</h2> <br>
<table style="background-color: grey" cellpadding="5px" border="2">
    <tr>
      <th>No</th>
      <th>Kode Pendaftaran</th>
      <th>Nama</th>
      <th>Jenis Kelamin</th>
      <th>Tempat/Tanggal Lahir</th>
      <th>Nomor Telepon</th>
      <th>Alamat</th>
      <th>Jenis Paket</th>
      <th>Total Bayar</th>
      <th>Kode Pegawai</th>
      <th>OPSI</th>
    </tr>

<?php
include "koneksi.php";
    $koneksi = mysqli_query($koneksi,"SELECT * FROM pendaftar");
    $no = 1;
    foreach ($koneksi as $data) {
?>

    <tr>
      <td><?php echo $no++; ?></td>
      <td><?php echo $data['kodedaftar']; ?></td>
      <td><?php echo $data['nama']; ?></td>
      <td><?php echo $data['JK']; ?></td>
      <td><?php echo $data['TTL']; ?></td>
      <td><?php echo $data['nmr_telepon']; ?></td>
      <td><?php echo $data['alamat']; ?></td>
      <td><?php echo $data['jenis_paket']; ?></td>
      <td><?php echo $data['total_bayar']; ?></td> 
      <td><?php echo $data['kodepegawai'];?></td>
      <td>  
        <a class="edit" href="daedit.php?kodedaftar=<?php echo $data['kodedaftar']; ?>">Edit</a>
        |<a class="hapus" href="dahapus.php?kodedaftar=<?php echo $data['kodedaftar']; ?>">Hapus</a>
        
      </td>
    </tr>
    
    <?php } ?>

    </table>
    <br>
    
  </div>
</center>
<br>
 <h5 align="center"><a class="tambah" href="datambah.php?kodedaftar=<?php echo $data['kodedaftar']; ?>">Tambah data</a></h5>
  
    <script src="js/cari.js"></script>
  </body>
</html>
