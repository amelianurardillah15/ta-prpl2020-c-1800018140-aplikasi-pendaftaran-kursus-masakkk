<!DOCTYPE html>
<html>
  <head>
     <title>Kursus Memasak Official</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:300,400,700,800|Open+Sans:300,400,700" rel="stylesheet">
    <script type="text/javascript" src="cari.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

            <h4><a href="index.html" class="site-logo">Home</a></h4>
        
    </head>

     <body style="background-image: url(images/slider-1.jpg);"><center>
      <h2> DATA PEGAWAI</h2> <br>
<table cellpadding="8px" style="background-color: grey">
    <tr>
      <th>No</th>
      <th>Kode Pegawai</th>
      <th>Nama</th>
      <th>Jenis Kelamin</th>
      <th>Alamat</th>
      <th>Username</th>
      <th>Password</th>
      <th>OPSI</th>
    </tr>
<?php
    include "koneksi.php";
    $koneksi = mysqli_query($koneksi,"SELECT * FROM pegawai");
    $no = 1;
    foreach ($koneksi as $data) {
?>

    <tr>
      <td><?php echo $no++; ?></td>
      <td><?php echo $data['kodepegawai'];?></td>
      <td><?php echo $data['nama'];?></td>
      <td><?php echo $data['JK'];?></td>
      <td><?php echo $data['alamat'];?></td>
      <td><?php echo $data['username'];?></td>
      <td><?php echo $data['password'];?></td>
      <td>  
        <a class="edit" href="pegedit.php?kodepegawai=<?php echo $data['kodepegawai']; ?>">Edit</a>
        |<a class="hapus" href="peghapus.php?kodepegawai=<?php echo $data['kodepegawai']; ?>">Hapus</a>
        |<a class="tambah" href="pegtambah.php?kodepegawai=<?php echo $data['kodepegawai']; ?>">Tambah Data</a>
      </td>
    </tr>

    <?php } ?>
    </table>
</center>
    <script src="js/cari.js"></script>
  </body>
</html>