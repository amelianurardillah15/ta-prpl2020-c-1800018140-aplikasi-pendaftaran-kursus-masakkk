-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Apr 2020 pada 10.24
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `kodepegawai` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `JK` enum('Perempuan','Laki-laki','','') NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`kodepegawai`, `nama`, `JK`, `alamat`) VALUES
('18109', 'Irawan', 'Laki-laki', 'Tanggerang'),
('18110', 'Dian', 'Perempuan', 'Bogor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar`
--

CREATE TABLE `pendaftar` (
  `kodedaftar` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `JK` enum('Perempuan','Laki-laki','','') NOT NULL,
  `TTL` date NOT NULL,
  `alamat` text NOT NULL,
  `nmr_telepon` int(30) NOT NULL,
  `jenis_paket` enum('Silver','Gold','Platinum','') NOT NULL,
  `total_bayar` enum('Rp.5000.000,00','Rp.10.000.000,00','Rp.15.000.000,00','') NOT NULL,
  `kodepegawai` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pendaftar`
--

INSERT INTO `pendaftar` (`kodedaftar`, `nama`, `JK`, `TTL`, `alamat`, `nmr_telepon`, `jenis_paket`, `total_bayar`, `kodepegawai`) VALUES
('111', 'Mita', 'Perempuan', '2000-04-05', 'Curup', 9897865, 'Silver', 'Rp.5000.000,00', '18109');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`kodepegawai`);

--
-- Indeks untuk tabel `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD PRIMARY KEY (`kodedaftar`),
  ADD KEY `kodepegawai` (`kodepegawai`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD CONSTRAINT `pendaftar_ibfk_1` FOREIGN KEY (`kodepegawai`) REFERENCES `pegawai` (`kodepegawai`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
